export function datetimeLocalSupported() {
    const input = document.createElement('input');
    input.setAttribute('type', 'datetime-local');
    return input.type === 'datetime-local';
}

export function dateSupported() {
    const input = document.createElement('input');
    input.setAttribute('type', 'date');
    return input.type === 'date';
}
