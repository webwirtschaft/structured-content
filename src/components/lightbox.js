import SimpleLightbox from "simplelightbox";

const wpscLightboxSelector = window.wpsc_lightbox_selector || '.has-wpsc-lightbox';

const wpscLightboxSettings = window.wpsc_lightbox_settings || {
    sourceAttr: 'data-image-url',
    close: true,
    showCounter: false,
    scrollZoom: false,
    loop: document.querySelectorAll(wpscLightboxSelector).length > 1,
};

window.wpsc_lightbox = new SimpleLightbox(wpscLightboxSelector, wpscLightboxSettings);
