/**
 * <button class="sc_recipe__printButton" data-target="sc_recipe--eabjl8cog">
 *                    Rezept ausdrucken                </button>
 */

const printButtons = document.querySelectorAll('.sc_recipe__printButton');

if (printButtons.length) {
    printButtons.forEach((button) => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            const recipeId = button.getAttribute('data-target');
            const recipe = document.querySelector(`#${recipeId}`);
            const recipeTitle = recipe.querySelector('.sc_recipe__head--text').innerText;
            const recipeContent = recipe.innerHTML;
            const printWindow = window.open('', 'PRINT');

            printWindow.document.write('<html><head><title>' + recipeTitle + '</title>');

            // if there is the class .sc_recipe__ingredients--checklist we need to remember the checked checkboxes
            const checklist = recipe.querySelector('.sc_recipe__ingredients--checklist');
            const checkedCheckboxes = [];

            if (checklist) {
                const checkboxes = checklist.querySelectorAll('input[type="checkbox"]');
                checkboxes.forEach((checkbox) => {
                    if (checkbox.checked) {
                        checkedCheckboxes.push(checkbox.value);
                    }
                })
            }

            // add css from wpsc_print_css_uri variable
            if (typeof window.wpsc_print_css_uri !== 'undefined') {
                printWindow.document.write(`<link rel="stylesheet" href="${window.wpsc_print_css_uri}" type="text/css" media="all" />`);
            } else {
                printWindow.document.write(`<style type="text/css" media="print">.no-print { display: none; }</style>`);
            }

            printWindow.document.write('</head><body >');
            printWindow.document.write(`<div class="sc_recipe__print">${recipeContent}</div>`);
            printWindow.document.write('</body></html>');

            // if there is the class .sc_recipe__ingredients--checklist we need to check the checkboxes again
            if (checklist) {
                const checkboxes = printWindow.document.querySelectorAll('.sc_recipe__ingredients--checklist input[type="checkbox"]');
                checkboxes.forEach((checkbox) => {
                    if (checkedCheckboxes.includes(checkbox.value)) {
                        checkbox.checked = true;
                    }
                })
            }


            setTimeout(() => {
                printWindow.document.close(); // necessary for IE >= 10
                printWindow.focus(); // necessary for IE >= 10*/

                printWindow.print();
                printWindow.close();
            }, 500);
        })
    })
}
