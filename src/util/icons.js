/**
 * Custom icons
 */

const iconColor = '#f03009';

const icons = {};

const iconClasses = 'dashicon components-structuredcontent-svg';

/**
 * Block Icons
 */

icons.faq =
    <svg className={iconClasses} width="20" height="20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M10 20a10 10 0 110-20 10 10 0 010 20zm2-13c0 .3-.2.8-.4 1L10 9.6c-.6.6-1 1.6-1 2.4v1h2v-1c0-.3.2-.8.4-1L13 9.4c.6-.6 1-1.6 1-2.4a4 4 0 10-8 0h2a2 2 0 114 0zm-3 8v2h2v-2H9z"/>
    </svg>;

icons.job =
    <svg className={iconClasses} width="20" height="20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M9 12H1v6a2 2 0 002 2h14a2 2 0 002-2v-6h-8v2H9v-2zm0-1H0V5c0-1.1.9-2 2-2h4V2a2 2 0 012-2h4a2 2 0 012 2v1h4a2 2 0 012 2v6h-9V9H9v2zm3-8V2H8v1h4z"/>
    </svg>;

icons.event =
    <svg className={iconClasses} width="20" height="20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M1 4c0-1.1.9-2 2-2h14a2 2 0 012 2v14a2 2 0 01-2 2H3a2 2 0 01-2-2V4zm2 2v12h14V6H3zm2-6h2v2H5V0zm8 0h2v2h-2V0zM5 9h2v2H5V9zm0 4h2v2H5v-2zm4-4h2v2H9V9zm0 4h2v2H9v-2zm4-4h2v2h-2V9zm0 4h2v2h-2v-2z"/>
    </svg>;

icons.person =
    <svg className={iconClasses} width="20" height="20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M0 2C0 .9.9 0 2 0h16a2 2 0 012 2v16a2 2 0 01-2 2H2a2 2 0 01-2-2V2zm7 4v2a3 3 0 106 0V6a3 3 0 00-6 0zm11 9.1a16 16 0 00-16 0V18h16v-2.9z"/>
    </svg>;

icons.course =
    <svg className={iconClasses} width="20" height="20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M3.3 8l6.7 4 10-6-10-6L0 6h10v2H3.3zM0 8v8l2-2.2V9.2L0 8zm10 12l-5-3-2-1.2v-6l7 4.2 7-4.2v6L10 20z"/>
    </svg>;

icons['local-business'] =
    <svg className={iconClasses} width="20" height="20" viewBox="0 0 20 20" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path d="M10.5 20H0V7L5 10.33V7L10 10.33V7L15 10.33V0H20V20H10.5Z"/>
    </svg>;

icons.recipe =
    <svg className={iconClasses} width="20" height="20" viewBox="0 0 20 20">
        <path
            d="M18 11V18C18 18.5304 17.7893 19.0391 17.4142 19.4142C17.0391 19.7893 16.5304 20 16 20C15.4696 20 14.9609 19.7893 14.5858 19.4142C14.2107 19.0391 14 18.5304 14 18V13H12V3C12 2.20435 12.3161 1.44129 12.8787 0.87868C13.4413 0.31607 14.2044 0 15 0L18 0V11ZM4 10C3.46957 10 2.96086 9.78929 2.58579 9.41421C2.21071 9.03914 2 8.53043 2 8V1C2 0.734784 2.10536 0.48043 2.29289 0.292893C2.48043 0.105357 2.73478 0 3 0C3.26522 0 3.51957 0.105357 3.70711 0.292893C3.89464 0.48043 4 0.734784 4 1V5H5V1C5 0.734784 5.10536 0.48043 5.29289 0.292893C5.48043 0.105357 5.73478 0 6 0C6.26522 0 6.51957 0.105357 6.70711 0.292893C6.89464 0.48043 7 0.734784 7 1V5H8V1C8 0.734784 8.10536 0.48043 8.29289 0.292893C8.48043 0.105357 8.73478 0 9 0C9.26522 0 9.51957 0.105357 9.70711 0.292893C9.89464 0.48043 10 0.734784 10 1V8C10 8.53043 9.78929 9.03914 9.41421 9.41421C9.03914 9.78929 8.53043 10 8 10V18C8 18.5304 7.78929 19.0391 7.41421 19.4142C7.03914 19.7893 6.53043 20 6 20C5.46957 20 4.96086 19.7893 4.58579 19.4142C4.21071 19.0391 4 18.5304 4 18V10Z"/>
    </svg>;

/**
 * UI Icons
 */

icons.remove =
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <rect width="24" height="24" rx="12" fill="#F0DADA"/>
        <path
            d="M17.25 7.8075L16.1925 6.75L12 10.9425L7.8075 6.75L6.75 7.8075L10.9425 12L6.75 16.1925L7.8075 17.25L12 13.0575L16.1925 17.25L17.25 16.1925L13.0575 12L17.25 7.8075Z"
            fill="#6A1B1B"/>
    </svg>;

icons.info =
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
        <path
            d="M2.93 17.07A10 10 0 1 1 17.07 2.93 10 10 0 0 1 2.93 17.07zM9 11v4h2V9H9v2zm0-6v2h2V5H9z"/>
    </svg>;

icons.openEye =
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
        <rect width="24" height="24" fill="#DAF0E8" rx="12"/>
        <path fill="#114332"
              d="M12 6.4c-3.8 0-7 2.3-8.3 5.6a8.9 8.9 0 0 0 16.6 0A8.9 8.9 0 0 0 12 6.4zm0 9.3a3.8 3.8 0 1 1 0-7.5 3.8 3.8 0 0 1 0 7.6zm0-6A2.2 2.2 0 0 0 9.7 12a2.2 2.2 0 0 0 2.3 2.3 2.2 2.2 0 0 0 2.3-2.3A2.2 2.2 0 0 0 12 9.7z"/>
    </svg>;

icons.closedEye =
    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24">
        <rect width="24" height="24" fill="#F2F3F3" rx="12"/>
        <path fill="#000"
              d="M12 8.3a3.8 3.8 0 0 1 3.5 5l2.2 2.3a8.9 8.9 0 0 0 2.5-3.6A8.9 8.9 0 0 0 9 6.9l1.6 1.6a3.6 3.6 0 0 1 1.4-.3zM4.5 6.2l1.7 1.7.4.4A8.9 8.9 0 0 0 3.8 12a8.9 8.9 0 0 0 8.2 5.6 8.8 8.8 0 0 0 3.3-.6l.3.3 2.2 2.2 1-1L5.4 5.2l-1 1zm4.1 4.1 1.2 1.2v.5a2.2 2.2 0 0 0 2.2 2.3l.5-.1 1.1 1.2a3.7 3.7 0 0 1-1.6.3 3.8 3.8 0 0 1-3.4-5.3zm3.3-.5 2.3 2.3A2.2 2.2 0 0 0 12 9.7z"/>
    </svg>;

icons.openSummary =
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24C18.6274 24 24 18.6274 24 12Z"
            fill="#DAF0E8"/>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M11.4144 14.8283L12.1215 15.5354L17.7783 9.87857L16.3641 8.46436L12.1215 12.707L7.87883 8.46436L6.46461 9.87857L11.4144 14.8283Z"
              fill="#114332"/>
    </svg>;

icons.closedSummary =
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
         xmlns="http://www.w3.org/2000/svg">
        <path
            d="M24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24C18.6274 24 24 18.6274 24 12Z"
            fill="#F2F3F3"/>
        <path fillRule="evenodd" clipRule="evenodd"
              d="M14.9499 12.7072L15.657 12.0001L10.0002 6.34326L8.58594 7.75748L12.8286 12.0001L8.58594 16.2428L10.0002 17.657L14.9499 12.7072Z"
              fill="black"/>
    </svg>;

icons.close =
    <svg xmlns="http://www.w3.org/2000/svg" fill="#fff" viewBox="0 0 24 24">
        <g data-name="Layer 2">
            <path
                d="M13.41 12l4.3-4.29a1 1 0 10-1.42-1.42L12 10.59l-4.29-4.3a1 1 0 00-1.42 1.42l4.3 4.29-4.3 4.29a1 1 0 000 1.42 1 1 0 001.42 0l4.29-4.3 4.29 4.3a1 1 0 001.42 0 1 1 0 000-1.42z"
                data-name="close"/>
        </g>
    </svg>;

icons.plus =
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
        <path
            d="M11 9V5H9v4H5v2h4v4h2v-4h4V9h-4zm-1 11a10 10 0 1 1 0-20 10 10 0 0 1 0 20z"/>
    </svg>;

icons.minus =
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
        <path d="M10 20a10 10 0 1 1 0-20 10 10 0 0 1 0 20zm5-11H5v2h10V9z"/>
    </svg>;

export {icons, iconColor};
