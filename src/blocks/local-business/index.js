/**
 * Internal dependencies
 */
import {iconColor, icons} from '../../util/icons.js';
import SC_Button from '../components/sc-buttons/index.js';
import InfoLabel from '../components/info-label/index.js';
import VisibleLabel from '../components/visible-label/index.js';
import {findNextFreeID, removeElement} from '../../util/helper.js';

/**
 * External dependencies
 */
import classNames from 'classnames';


/**
 /* WordPress dependencies
 */
import {__, _x} from '@wordpress/i18n';
import {Fragment} from '@wordpress/element'
import {
    AlignmentToolbar,
    BlockControls,
    InspectorControls,
    MediaUpload,
    PlainText,
    RichText,
} from '@wordpress/block-editor';
import {PanelBody, PanelRow, SelectControl, TextControl, ToggleControl} from '@wordpress/components';
import {registerBlockType} from '@wordpress/blocks';

/* Block constants
 */
const name = 'local-business';
const title = _x('Local Business', 'Local Business block title', 'structured-content');
const icon = {src: icons[name], foreground: iconColor};

const keywords = [_x('local business search', 'local business search', 'structured-content'), _x('structured-content', 'structured content element local business', 'structured-content')];

const blockAttributes = {
    business_name: {
        type: 'string',
        default: '',
    },
    description: {
        type: 'string',
        default: '',
    },
    title_tag: {
        type: 'string',
        default: 'h2',
    },
    generate_title_id: {
        type: 'boolean',
        default: false,
    },
    custom_title_id: {
        type: 'string',
        default: '',
    },
    currencies_accepted: {
        type: 'string',
        default: '',
    },
    founding_date: {
        type: 'string',
        default: '',
    },
    street_address: {
        type: 'string',
        default: '',
    },
    address_locality: {
        type: 'string',
        default: '',
    },
    postal_code: {
        type: 'string',
        default: '',
    },
    price_range: {
        type: 'string',
        default: '',
    },
    address_region: {
        type: 'string',
        default: '',
    },
    address_country: {
        type: 'string',
        default: '',
    },
    latitude: {
        type: 'string',
        default: '',
    },
    longitude: {
        type: 'string',
        default: '',
    },
    has_map: {
        type: 'string',
        default: '',
    },
    contact_email: {
        type: 'string',
        default: '',
    },
    contact_telephone: {
        type: 'string',
        default: '',
    },
    contact_url: {
        type: 'string',
        default: '',
    },
    email: {
        type: 'string',
        default: '',
    },
    website: {
        type: 'string',
        default: '',
    },
    telephone: {
        type: 'string',
        default: '',
    },
    image_id: {
        type: 'number',
        default: '',
    },
    imageAlt: {
        type: 'string',
        default: '',
    },
    image_thumbnail: {
        type: 'string',
        default: '',
    },
    logo_id: {
        type: 'number',
        default: '',
    },
    logoAlt: {
        type: 'string',
        default: '',
    },
    logo_thumbnail: {
        type: 'string',
        default: '',
    },
    opening_hours: {
        type: 'array',
        default: [],
    },
    same_as: {
        type: 'array',
        default: [],
    },
    contact_type: {
        type: 'string',
    },
    textAlign: {
        type: 'string',
    },
    html: {
        type: 'bool',
        default: true,
    },
};
/* Register: aa Gutenberg Block.
/*
/* Registers a new block provided a unique name and an object defining its
/* behavior. Once registered, the block is made editor as an option to any
/* editor interface where blocks are implemented.
/*
/* @link https://wordpress.org/gutenberg/handbook/block-api/
/* @param  {string}   name     Block name.
/* @param  {Object}   settings Block settings.
/* @return {?WPBlock}          The block, if it has been successfully
/*                             registered otherwise `undefined`.
*/
registerBlockType(`structured-content/${name}`, {
    title, // Block title.
    icon, category: 'structured-content', keywords,

    attributes: blockAttributes,

    supports: {
        align: ['wide', 'full'], stackedOnMobile: true,
    },

    edit: ({attributes, className, isSelected, setAttributes}) => {
        const {
            address_country,
            address_locality,
            address_region,
            align,
            business_name,
            contact_email,
            contact_telephone,
            contact_type,
            contact_url,
            custom_title_id,
            currencies_accepted,
            description,
            email,
            founding_date,
            generate_title_id,
            has_map,
            html,
            image_thumbnail,
            image_id,
            latitude,
            logo_thumbnail,
            logo_id,
            longitude,
            opening_hours,
            postal_code,
            price_range,
            same_as,
            street_address,
            telephone,
            textAlign,
            title_tag,
            website,
        } = attributes;

        function onImageSelect(imageObject) {
            setAttributes({
                image_id: imageObject.id,
                imageAlt: imageObject.alt,
                image_thumbnail: imageObject.sizes?.thumbnail?.url || imageObject.url,
            });
        }

        function onRemoveImage() {
            setAttributes({
                image_id: null, imageAlt: null, image_thumbnail: '',
            });
        }

        function onLogoSelect(imageObject) {
            setAttributes({
                logo_id: imageObject.id,
                logoAlt: imageObject.alt,
                logo_thumbnail: imageObject.sizes?.thumbnail?.url || imageObject.url,
            });
        }

        function onRemoveLogo() {
            setAttributes({
                logo_id: null, logoAlt: null, logo_thumbnail: '',
            });
        }

        function addToOpeningHours() {
            const id = findNextFreeID(opening_hours);
            setAttributes({
                opening_hours: [...opening_hours, {id, opening: ''}],
            });
        }

        function removeFromOpeningHours(id) {
            setAttributes({opening_hours: removeElement(id, opening_hours)});
        }

        function addToSameAs() {
            const id = findNextFreeID(same_as);
            setAttributes({
                same_as: [...same_as, {id, url: ''}],
            });
        }

        function removeFromSameAs(id) {
            setAttributes({same_as: removeElement(id, same_as)});
        }

        function opening_hoursSave() {
            let list = '';
            opening_hours.map(function (value, index) {
                if (value.opening !== '') {
                    list += `${value.opening},`;
                }
            });
            list = list.substr(0, list.length - 1);
            setAttributes({colleague: list});
        }

        function sameAsSave() {
            let list = '';
            same_as.map(function (value, index) {
                if (value.url !== '') {
                    list += `${value.url},`;
                }
            });
            list = list.substr(0, list.length - 1);
            setAttributes({colleague: list});
        }

        function beforeSave() {
            sameAsSave();
            opening_hoursSave();
        }

        const openingHours = opening_hours.sort(function (a, b) {
            return a.index - b.index;
        }).map((data, index) => {
            return (<div
                style={{
                    display: 'grid', gridTemplateColumns: '3fr auto', gridColumnGap: 5, lineHeight: 1,
                }}
                key={`opening-${index}`}
            >
                <TextControl
                    type="text"
                    value={data.opening}
                    placeholder={data.opening !== '' ? data.opening : _x('Mo 10:00-16:00', 'opening hours placeholder', 'structured-content',)}
                    keepplaceholderonfocus="true"
                    className={`wp-block-structured-content-local-business__repeater-${data.id}`}
                    onChange={(value) => {
                        setAttributes((opening_hours[index] = {id: data.id, opening: value}),);
                        opening_hoursSave();
                    }}
                />
                <div>
                    <SC_Button
                        action={() => removeFromOpeningHours(data.id)}
                        icon={true}
                        className="inline"
                        differentIcon={icons.minus}
                    />
                </div>
            </div>);
        });

        const sameAsUrls = same_as.sort(function (a, b) {
            return a.index - b.index;
        }).map((data, index) => {
            return (<div
                style={{
                    display: 'grid', gridTemplateColumns: '3fr auto', gridColumnGap: 5, lineHeight: 1,
                }}
                key={`sameAs-${index}`}
            >
                <TextControl
                    type="text"
                    value={data.url}
                    placeholder={data.url !== '' ? data.url : _x('https://xyz.com/me.html', 'Enter a additional profile URL to the local business.', 'structured-content')}
                    keepplaceholderonfocus="true"
                    className={`wp-block-structured-content-local-business__repeater-${data.id}`}
                    onChange={(value) => {
                        setAttributes((same_as[index] = {id: data.id, url: value}),);
                        sameAsSave();
                    }}
                />
                <div>
                    <SC_Button
                        action={() => removeFromSameAs(data.id)}
                        icon={true}
                        className="inline"
                        differentIcon={icons.minus}
                    />
                </div>
            </div>);
        });

        return [<Fragment>
            {isSelected && (<Fragment>
                <BlockControls>
                    <AlignmentToolbar
                        value={textAlign}
                        onChange={(nextTextAlign) => setAttributes({textAlign: nextTextAlign})}
                    />
                </BlockControls>
            </Fragment>)}
            {isSelected && (<Fragment>
                <InspectorControls>
                    <PanelBody>
                        <SelectControl
                            label={_x('Title tag', 'Select a tag of the FAQ title.', 'structured-content')}
                            value={title_tag}
                            options={[
                                {label: 'H2', value: 'h2'},
                                {label: 'H3', value: 'h3'},
                                {label: 'H4', value: 'h4'},
                                {label: 'H5', value: 'h5'},
                                {label: 'p', value: 'p'},
                            ]}
                            onChange={(title_tag) => {
                                setAttributes({title_tag});
                            }}
                        />
                        <PanelRow>
                            <ToggleControl
                                label={_x('Generate title ID', 'Generate a title ID', 'structured-content')}
                                help={_x('Generate a title ID for the FAQ title.', 'Generate a title ID', 'structured-content')}
                                checked={generate_title_id}
                                onChange={() => setAttributes({generate_title_id: !generate_title_id})}
                            />
                        </PanelRow>
                        {generate_title_id && (<PanelRow>
                            <TextControl
                                label={_x('Custom title ID', 'Custom title ID', 'structured-content')}
                                className="w-100"
                                value={custom_title_id}
                                help={_x('Custom title ID for the Job title.', 'Custom title ID', 'structured-content')}
                                onChange={(custom_title_id) => setAttributes({custom_title_id})}
                            />
                        </PanelRow>)}
                    </PanelBody>
                </InspectorControls>
            </Fragment>)}
            <section
                className={classNames(
                    className,
                    align && `align${align}`,
                    'sc_card'
                )}
                style={{
                    textAlign,
                }}
            >
                <div className="sc_toggle-bar">
                    <div onClick={() => {
                        setAttributes({html: !html});
                    }}>
                        <VisibleLabel visible={html}/>
                    </div>
                    <InfoLabel
                        url="https://developers.google.com/search/docs/advanced/structured-data/local-business"/>
                </div>
                <div>
                    {wp.element.createElement(title_tag, {className: 'business-name'},
                        <PlainText
                            className="wp-block-structured-content-local-business__business_name"
                            keepplaceholderonfocus="true"
                            onChange={(business_name) => setAttributes({business_name})}
                            placeholder={_x('Your Business Name ', 'Name of your Business', 'structured-content')}
                            tag={title_tag}
                            value={business_name}
                        />,
                    )}
                    <div className="description" itemProp="text">
                        <RichText
                            className="wp-block-structured-content-course__description"
                            keepplaceholderonfocus="true"
                            onChange={(description) => setAttributes({description})}
                            placeholder={_x('Describe your business', 'Enter a detailed description of the business.', 'structured-content')}
                            value={description}
                        />
                    </div>
                </div>
                <div>
                    <div className="sc_row mt-4" style={{marginTop: 15}}>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Business', 'Some details about the local business.', 'structured-content')}
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('E-Mail', 'Actual email of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="email"
                                    value={email}
                                    placeholder={_x('Please enter your business email here.', 'Concrete email address of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__email"
                                    onChange={(email) => setAttributes({email})}
                                />
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Telephone', 'Actual telephone of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="tel"
                                    value={telephone}
                                    placeholder={_x('Please enter your business telephone here.', 'Concrete telephone number of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__telephone"
                                    onChange={(telephone) => setAttributes({telephone})}
                                />
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Website', 'Actual website of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="url"
                                    value={website}
                                    placeholder={_x('Please enter your business website here.', 'Concrete website of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__website"
                                    onChange={(website) => setAttributes({website})}
                                />
                            </div>
                            <div className="sc_row">

                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Image', 'Image of the local business.', 'structured-content')}
                                    </div>
                                    <div>
                                        {!image_thumbnail ? (<MediaUpload
                                            onSelect={onImageSelect}
                                            type="image"
                                            value={image_id}
                                            render={({open}) => (<SC_Button
                                                action={open}
                                                className="no-margin-top"
                                            >
                                                {_x('Add Image', 'Select image of the local business now.', 'structured-content')}
                                            </SC_Button>)}
                                        />) : (<div>
                                            <div className="image-wrapper">
                                                <img itemProp="image" src={image_thumbnail}/>
                                            </div>
                                            <SC_Button
                                                action={onRemoveImage}
                                                className="no-margin-top"
                                            >
                                                {_x('Remove Image', 'Remove the image of the local business.', 'structured-content')}
                                            </SC_Button>
                                        </div>)}
                                    </div>
                                </div>
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Logo', 'Logo of the business.', 'structured-content')}
                                    </div>
                                    <div>
                                        {!logo_thumbnail ? (<MediaUpload
                                            onSelect={onLogoSelect}
                                            type="image"
                                            value={logo_id}
                                            render={({open}) => (<SC_Button
                                                action={open}
                                                className="no-margin-top"
                                            >
                                                {_x('Add Logo', 'Select logo of the business now.', 'structured-content')}
                                            </SC_Button>)}
                                        />) : (<div>
                                            <div className="image-wrapper">
                                                <img itemProp="logo" src={logo_thumbnail}/>
                                            </div>
                                            <SC_Button
                                                action={onRemoveLogo}
                                                className="no-margin-top"
                                            >
                                                {_x('Remove Logo', 'Remove the logo of the business.', 'structured-content')}
                                            </SC_Button>
                                        </div>)}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Contact', 'How to get in touch with the local business?', 'structured-content')}
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Contact Type', 'Contact Type of the business.', 'structured-content')}
                                </div>
                                <TextControl
                                    placeholder={_x('Customer Support', 'Concrete Contact Type of the business', 'structured-content')}
                                    value={contact_type}
                                    type="text"
                                    className="wp-block-structured-content-local-business__contact_type"
                                    onChange={(contact_type) => setAttributes({contact_type})}
                                />
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('E-Mail', 'E-Mail of the local business.', 'structured-content')}
                                </div>
                                <TextControl
                                    placeholder={_x('jane-doe@xyz.edu', 'Concrete email of the local business.', 'structured-content')}
                                    value={contact_email}
                                    type="email"
                                    className="wp-block-structured-content-local-business__contact_email"
                                    onChange={(contact_email) => setAttributes({contact_email})}
                                />
                            </div>
                            <div className="sc_input-group" style={{marginTop: 15}}>
                                <div className="sc_input-label">
                                    {_x('URL', 'URL to a profile of the local business.', 'structured-content')}
                                </div>
                                <TextControl
                                    placeholder={_x('http://www.janedoe.com', 'Concrete URL of the local business.', 'structured-content',)}
                                    value={contact_url}
                                    type="url"
                                    className="wp-block-structured-content-local-business__contact_url"
                                    onChange={(contact_url) => setAttributes({contact_url})}
                                />
                            </div>
                            <div className="sc_input-group" style={{marginTop: 15}}>
                                <div className="sc_input-label">
                                    {_x('Telephone', 'Telephone number of the local business.', 'structured-content')}
                                </div>
                                <TextControl
                                    placeholder={_x('(425) 123-4567', 'Enter concrete telephone number of the local business.', 'structured-content')}
                                    value={contact_telephone}
                                    type="tel"
                                    className="wp-block-structured-content-local-business__contact_telephone"
                                    onChange={(contact_telephone) => setAttributes({contact_telephone})}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="sc_row" style={{marginTop: 15}}>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Address', 'Address of the local business.', 'structured-content')}
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Street', 'Insert the street name where the local business live.', 'structured-content')}
                                </div>
                                <TextControl
                                    placeholder={_x('Any Street 3A', 'The concrete street name where the local business live.', 'structured-content')}
                                    type="text"
                                    value={street_address}
                                    className="wp-block-structured-content-local-business__street_address"
                                    onChange={(street_address) => setAttributes({street_address})}
                                />
                            </div>
                            <div className="sc_row">
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Postal Code', 'Insert postal code where the local business live.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={_x('Any postal code', 'The concrete postal code where the local business live.', 'structured-content')}
                                        type="text"
                                        value={postal_code}
                                        className="wp-block-structured-content-local-business__postal_code"
                                        onChange={(postal_code) => setAttributes({postal_code})}
                                    />
                                </div>
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Locality', 'Enter the place name where the local business live.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={_x('Any city', 'The concrete city name where the local business live.', 'structured-content')}
                                        type="text"
                                        value={address_locality}
                                        className="wp-block-structured-content-local-business__address_locality"
                                        onChange={(address_locality) => setAttributes({address_locality})}
                                    />
                                </div>
                            </div>
                            <div className="sc_row">
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Country ISO Code', 'Enter the iso code of the country where the local business live.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('US', 'structured-content')}
                                        type="text"
                                        value={address_country}
                                        className="wp-block-structured-content-local-business__address_country"
                                        onChange={(address_country) => setAttributes({address_country})}
                                    />
                                </div>
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Region ISO Code', 'Enter the iso code of the region where the local business live.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={__('US-CA', 'structured-content')}
                                        type="text"
                                        value={address_region}
                                        className="wp-block-structured-content-local-business__address_region"
                                        onChange={(address_region) => setAttributes({address_region})}
                                    />
                                </div>
                            </div>
                            <div className="sc_row">
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Latitude', 'Enter the latitude of the business.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        type="text"
                                        value={latitude}
                                        className="wp-block-structured-content-local-business__latitude"
                                        onChange={(latitude) => setAttributes({latitude})}
                                    />
                                </div>
                                <div className="sc_input-group">
                                    <div className="sc_input-label">
                                        {_x('Longitude', 'Enter the longitude of the business.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        type="text"
                                        value={longitude}
                                        className="wp-block-structured-content-local-business__longitude"
                                        onChange={(longitude) => setAttributes({longitude})}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Meta', 'Details about the business.', 'structured-content')}
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Founding Date', 'Actual founding date of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="text"
                                    value={founding_date}
                                    placeholder={_x('YYYY-MM-DD or YYYY', 'Concrete founding date number of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__founding_date"
                                    onChange={(founding_date) => setAttributes({founding_date})}
                                />
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Currencies Accepted', 'Actual accepted currencies of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="text"
                                    value={currencies_accepted}
                                    placeholder={_x('USD or EUR', 'Concrete accepted currencies number of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__currencies_accepted"
                                    onChange={(currencies_accepted) => setAttributes({currencies_accepted})}
                                />
                            </div>
                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Price Range', 'Actual price range of the business', 'structured-content')}
                                </div>
                                <TextControl
                                    type="text"
                                    value={price_range}
                                    placeholder={_x('$-$$$$$', 'Concrete price range number of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__price_range"
                                    onChange={(price_range) => setAttributes({price_range})}
                                />
                            </div>

                            <div className="sc_input-group">
                                <div className="sc_input-label">
                                    {_x('Has Map', 'Map Link of the business.', 'structured-content')}
                                </div>
                                <TextControl
                                    type="text"
                                    value={has_map}
                                    placeholder={_x('https://goo.gl/maps/…', 'Concrete map Link of the business.', 'structured-content',)}
                                    className="wp-block-structured-content-local-business__has_map"
                                    onChange={(has_map) => setAttributes({has_map})}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="sc_row" style={{marginTop: 15}}>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Opening Hours', 'Business Opening Hours.', 'structured-content')}
                            </div>
                            <div className="sc_input-group" style={{marginTop: 15}}>
                                <div className="sc_input-label">
                                    {_x('Opening Hours', 'Business Opening Hours.', 'structured-content')}
                                </div>
                                <div>
                                    <div>{openingHours}</div>
                                    <SC_Button action={addToOpeningHours} icon={true}>
                                        {_x('Add One', 'Add more opening hours.', 'structured-content')}
                                    </SC_Button>
                                </div>
                            </div>
                        </div>
                        <div className="sc_grey-box">
                            <div className="sc_box-label">
                                {_x('Same As', 'Profile URL of the local business.', 'structured-content')}
                            </div>
                            <div className="sc_input-group" style={{marginTop: 15}}>
                                <div className="sc_input-label">
                                    {_x('URL', 'URL to a profile of the local business.', 'structured-content')}
                                </div>
                                <div>
                                    <div>{sameAsUrls}</div>
                                    <SC_Button action={addToSameAs} icon={true}>
                                        {_x('Add One', 'Add another URL to a profile of the local business.', 'structured-content')}
                                    </SC_Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </Fragment>];
    },

    /**
     /* The save function defines the way in which the different attributes should be combined
     /* into the final markup, which is then serialized by Gutenberg into post_content.
     /*
     /* The "save" property must be specified and must be a valid function.
     /*
     /* @link https://wordpress.org/gutenberg/handbook/block-api/block-edit-save/
     *
     * @param  props
     */
    save(props) {
        return null;
    },
});
