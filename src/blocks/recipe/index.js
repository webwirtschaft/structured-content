/**
 * Internal dependencies
 */
import {iconColor, icons} from 'util/icons';
import SC_Button from '../components/sc-buttons/index.js';
import InfoLabel from '../components/info-label/index.js';

/**
 * External dependencies
 */
import classNames from 'classnames';
import VisibleLabel from "../components/visible-label/index.js";
import DurationInput from "../components/duration-input/index.jsx";
import VideoInput from "../components/video-input/index.jsx";

/**
 * WordPress dependencies
 */
import {__, _x} from '@wordpress/i18n';

const {Fragment} = wp.element;
const {
    AlignmentToolbar,
    InspectorControls,
    BlockControls,
    MediaUpload,
    PlainText,
    RichText,
} = wp.blockEditor;
const {
    PanelRow,
    PanelBody,
    SelectControl,
    TextControl,
    ToggleControl,
    FormTokenField,
    Placeholder,
    CheckboxControl
} = wp.components;
const {registerBlockType} = wp.blocks;

const videoDefaults = {
    name: undefined,
    description: undefined,
    thumbnailUrl: undefined,
    contentUrl: undefined,
    titleTag: undefined,
    mediaVideo: undefined,
    initialTab: undefined,
    settings: {
        autoplay: false,
        loop: false,
        muted: false,
        controls: true,
    }
}

/**
 * Block constants
 */
const name = 'recipe';
const title = __('Recipe', 'structured-content');
const icon = {src: icons[name], foreground: iconColor};

const keywords = [
    _x('recipe', 'recipe', 'structured-content')
];

const blockAttributes = {
    titleTag: {
        type: 'string',
        default: 'h2',
    },
    title: {
        type: 'string',
    },
    generateTitleId: {
        type: 'boolean',
        default: false,
    },
    showPrintButton: {
        type: 'boolean',
        default: true,
    },
    printButtonText: {
        type: 'string',
        default: _x('Print Recipe', 'Print Recipe Button Text', 'structured-content'),
    },
    textAlign: {
        type: 'string',
    },
    imageID: {
        type: 'string',
    },
    coverImageSize: {
        type: 'string',
        default: 'medium',
    },
    thumbnailImageUrl: {
        type: 'string',
    },
    description: {
        type: 'string',
        default: '',
    },
    prepTime: {
        type: 'string',
        default: '',
    },
    cookTime: {
        type: 'string',
        default: '',
    },
    totalTime: {
        type: 'string',
        default: '',
    },
    keywords: {
        type: 'array',
        default: [],
    },
    nutrition: {
        type: 'array',
        default: [],
    },
    recipeYield: {
        type: 'string',
        default: '',
    },
    recipeCategory: {
        type: 'string',
        default: '',
    },
    recipeCuisine: {
        type: 'string',
        default: '',
    },
    recipeIngredient: {
        type: 'array',
        default: [],
    },
    recipeInstructions: {
        type: 'array',
        default: [],
    },
    instructionImageSize: {
        type: 'string',
        default: 'medium',
    },
    instructionImageLightbox: {
        type: 'boolean',
        default: false,
    },
    ingredientsAsChecklist: {
        type: 'boolean',
        default: false,
    },
    author: {
        type: 'object',
        default: {
            name: undefined,
            type: undefined,
        }
    },
    hasVideo: {
        type: 'boolean',
        default: false,
    },
    video: {
        type: 'object',
        default: videoDefaults,
    },
    version: {
        type: Number,
    },
    visible: {
        type: 'bool',
        default: true,
    },
    randomBlockId: {
        type: 'string',
        default: '',
    },
};


registerBlockType(`structured-content/${name}`, {
    title,
    icon,
    category: 'structured-content',
    keywords,
    attributes: blockAttributes,
    providesContext: {
        'structured-content/titleTag': 'titleTag',
        'structured-content/show_summary': 'summary',
        'structured-content/generateTitleId': 'generateTitleId',
    },
    supports: {
        align: ['wide', 'full'],
        stackedOnMobile: true,
    },
    edit: ({
               attributes,
               className,
               isSelected,
               setAttributes,
           }) => {
        const {
            align,
            visible,
            textAlign,
            version,
            titleTag,
            title,
            generateTitleId,
            coverImageSize,
            showPrintButton,
            printButtonText,
            imageID,
            thumbnailImageUrl,
            description,
            prepTime,
            cookTime,
            totalTime,
            keywords,
            nutrition,
            recipeYield,
            recipeCategory,
            recipeCuisine,
            recipeIngredient,
            recipeInstructions,
            hasVideo,
            video,
            author,
            randomBlockId,
            ingredientsAsChecklist,
            instructionImageSize,
            instructionImageLightbox
        } = attributes;


        setAttributes({
            version: 1,
        });

        const onImageSelect = (image) => {
            setAttributes({
                imageID: image.id,
                thumbnailImageUrl: image.sizes[coverImageSize].url || image.url,
            });
        }

        const onImageRemove = (image) => {
            setAttributes({
                imageID: '',
                thumbnailImageUrl: '',
            });
        }


        const onNutritionRemove = (index) => {
            const newNutrition = [...nutrition];
            newNutrition.splice(index, 1);
            setAttributes({nutrition: newNutrition});
        }

        const onNutritionAdd = () => {
            const newNutrition = [...nutrition];
            newNutrition.push({
                type: 'calories',
                value: '',
            });
            setAttributes({nutrition: newNutrition});
        }

        const onNutritionValueChange = (value, index) => {
            const newNutrition = [...nutrition];
            newNutrition[index].value = value;
            setAttributes({nutrition: newNutrition});
        }

        const onNutritionTypeChange = (type, index) => {
            const newNutrition = [...nutrition];
            newNutrition[index].type = type;
            setAttributes({nutrition: newNutrition});
        }

        const onIngredientChange = (value, index) => {
            const newIngredients = [...recipeIngredient];
            newIngredients[index] = value;
            setAttributes({recipeIngredient: newIngredients});
        }

        const onIngredientRemove = (index) => {
            const newIngredients = [...recipeIngredient];
            newIngredients.splice(index, 1);
            setAttributes({recipeIngredient: newIngredients});
        }

        const onIngredientAdd = () => {
            const newIngredients = [...recipeIngredient];
            newIngredients.push('');
            setAttributes({recipeIngredient: newIngredients});
        }

        const onInstructionChange = (value, index) => {
            const newInstructions = [...recipeInstructions];
            newInstructions[index].text = value;
            setAttributes({recipeInstructions: newInstructions});
        }

        const onInstructionRemove = (index) => {
            const newInstructions = [...recipeInstructions];
            newInstructions.splice(index, 1);
            setAttributes({recipeInstructions: newInstructions});
        }

        const onInstructionAdd = () => {
            const newInstructions = [...recipeInstructions];
            newInstructions.push({
                text: '',
                image: undefined,
            });
            setAttributes({recipeInstructions: newInstructions});
        }

        const onInstructionImageRemove = (index) => {
            const newInstructions = [...recipeInstructions];
            newInstructions[index].image = undefined;
            setAttributes({recipeInstructions: newInstructions});
        }

        const onInstructionImageChange = (image, index) => {
            const newInstructions = [...recipeInstructions];
            newInstructions[index].image = image;
            setAttributes({recipeInstructions: newInstructions});
        }

        const onVideoChange = (video) => {
            setAttributes({video});
        }

        const changeTitle = (value) => {
            setAttributes({
                title: value,
                randomBlockId: Math.random().toString(36).substr(2, 9),
            })
        }

        /**
         * Types of Nutrition
         */
        const nutritionTypes = [
            {label: __('Calories', 'structured-content'), value: 'calories'},
            {label: __('Carbohydrates', 'structured-content'), value: 'carbohydrates'},
            {label: __('Protein', 'structured-content'), value: 'protein'},
            {label: __('Fat', 'structured-content'), value: 'fat'},
            {label: __('Saturated Fat', 'structured-content'), value: 'saturatedFat'},
            {label: __('Monounsaturated Fat', 'structured-content'), value: 'monounsaturatedFat'},
            {label: __('Polyunsaturated Fat', 'structured-content'), value: 'polyunsaturatedFat'}
        ];

        /**
         * Get WordPress Registered Image Sizes
         */
        const blockEditorSettings = wp.data.select('core/block-editor').getSettings();
        const imageSizes = blockEditorSettings.imageSizes.map((imageSize) => {
            return {
                label: imageSize.name,
                value: imageSize.slug,
            };
        });

        return [
            <Fragment>
                {isSelected && (
                    <Fragment>
                        <BlockControls>
                            <AlignmentToolbar
                                value={textAlign}
                                onChange={(nextTextAlign) => setAttributes(
                                    {textAlign: nextTextAlign})}
                            />
                        </BlockControls>
                    </Fragment>
                )}
                {isSelected && (
                    <Fragment>
                        <InspectorControls>
                            <PanelBody title={_x('Title Settings', 'Recipe Settings', 'structured-content')}>
                                <PanelRow>
                                    <SelectControl
                                        label={_x('Title tag', 'Select a tag of the Recipe title.', 'structured-content')}
                                        help={_x('Select a tag of the Recipe title.', 'Select a tag of the Recipe title.', 'structured-content')}
                                        value={titleTag}
                                        options={[
                                            {label: 'H2', value: 'h2'},
                                            {label: 'H3', value: 'h3'},
                                            {label: 'H4', value: 'h4'},
                                            {label: 'H5', value: 'h5'},
                                            {label: 'p', value: 'p'},
                                        ]}
                                        onChange={(titleTag) => {
                                            setAttributes({titleTag});
                                        }}
                                    />
                                </PanelRow>

                                <PanelRow>
                                    <ToggleControl
                                        label={_x('Generate title ID', 'Generate a title ID', 'structured-content')}
                                        help={_x('Generate a title ID for the Recipe title.', 'Generate a title ID', 'structured-content')}
                                        checked={generateTitleId}
                                        onChange={() => setAttributes({generateTitleId: !generateTitleId})}
                                    />
                                </PanelRow>

                                <PanelRow>
                                    <SelectControl
                                        label={_x('Cover Image Size', 'Select an image size.', 'structured-content')}
                                        help={_x('Select an image size for the cover image.', 'Select an image size.', 'structured-content')}
                                        value={coverImageSize}
                                        options={imageSizes}
                                        onChange={(coverImageSize) => {
                                            setAttributes({coverImageSize});
                                        }}
                                    />
                                </PanelRow>

                            </PanelBody>
                            <PanelBody title={_x('Tags Settings', 'Recipe Settings', 'structured-content')}
                                       initialOpen={false}>

                                <PanelRow>
                                    {/* Recipe Keywords */}
                                    <FormTokenField
                                        label={_x('Keywords', 'Keywords of the recipe.', 'structured-content')}
                                        keepplaceholderonfocus="true"
                                        className="wp-block-structured-content-recipe__keywords"
                                        placeholder={_x('Enter your recipe keywords here… ', 'Keywords of the recipe.', 'structured-content')}
                                        value={keywords}
                                        suggestions={[]}
                                        onChange={(value) => setAttributes({keywords: value})}
                                    />
                                </PanelRow>

                            </PanelBody>
                            <PanelBody title={_x('Print Settings', 'Recipe Settings', 'structured-content')}
                                       initialOpen={false}>


                                <PanelRow>
                                    <ToggleControl
                                        label={_x('Print Button', 'Show the print recipe button', 'structured-content')}
                                        help={_x('Shows a button to print the recipe.', 'Show the print recipe button', 'structured-content')}
                                        checked={showPrintButton}
                                        onChange={() => setAttributes({showPrintButton: !showPrintButton})}
                                    />
                                </PanelRow>

                                {showPrintButton && (
                                    <PanelRow>
                                        <TextControl
                                            label={_x('Print Button Text', 'Text of the print button.', 'structured-content')}
                                            help={_x('Text of the print button.', 'Text of the print button.', 'structured-content')}
                                            value={printButtonText}
                                            onChange={(value) => setAttributes({printButtonText: value})}
                                        />
                                    </PanelRow>
                                )}

                            </PanelBody>

                            <PanelBody title={_x('Instruction Settings', 'Recipe Settings', 'structured-content')}
                                       initialOpen={false}>
                                <PanelRow>
                                    <SelectControl
                                        label={_x('Image Size', 'Select an image size.', 'structured-content')}
                                        help={_x('Select an image size for the instruction images.', 'Select an image size.', 'structured-content')}
                                        value={instructionImageSize}
                                        options={imageSizes}
                                        onChange={(instructionImageSize) => {
                                            setAttributes({instructionImageSize});
                                        }}
                                    />
                                </PanelRow>

                                <PanelRow>
                                    <ToggleControl
                                        label={_x('Lightbox', 'Lightbox for the instruction images.', 'structured-content')}
                                        help={_x('Enable lightbox for the instruction images.', 'Lightbox for the instruction images.', 'structured-content')}
                                        checked={instructionImageLightbox}
                                        onChange={() => setAttributes({instructionImageLightbox: !instructionImageLightbox})}
                                    />
                                </PanelRow>
                            </PanelBody>

                            <PanelBody title={_x('Ingredients Settings', 'Recipe Settings', 'structured-content')}
                                       initialOpen={false}>
                                <PanelRow>
                                    <ToggleControl
                                        label={_x('Display Ingredients as Checklist', 'Display Ingredients as Checklist', 'structured-content')}
                                        help={_x('Changes the display of the ingredients to a checklist.', 'Display Ingredients as Checklist', 'structured-content')}
                                        checked={ingredientsAsChecklist}
                                        onChange={() => setAttributes({ingredientsAsChecklist: !ingredientsAsChecklist})}
                                    />
                                </PanelRow>
                            </PanelBody>
                        </InspectorControls>
                    </Fragment>
                )}
                <section
                    className={classNames(
                        className,
                        align && `align${align}`,
                        'sc_card'
                    )}
                    style={{
                        textAlign,
                    }}>
                    <div className="sc_toggle-bar">
                        <VisibleLabel onClick={() => setAttributes({visible: !visible})} visible={visible}/>
                        <InfoLabel
                            url="https://developers.google.com/search/docs/data-types/recipe"/>
                    </div>

                    <div className="sc_recipe-image__wrapper">
                        {!thumbnailImageUrl
                            ? <MediaUpload
                                onSelect={(media) => onImageSelect(media)}
                                type="image"
                                value={imageID}
                                allowedTypes={['image']}
                                render={({open}) => (
                                    <div className="sc_recipe-image-placeholder">
                                        <SC_Button action={open}>
                                            {_x('Add Image',
                                                'Illustrate your Recipe with a meaningful image.',
                                                'structured-content')}
                                        </SC_Button>
                                    </div>
                                )}
                            />
                            : <figure className="sc_fs_recipe__figure">
                                <img
                                    itemProp="image"
                                    className="sc_fs_recipe__image"
                                    src={thumbnailImageUrl}
                                    alt={title}
                                />
                                <SC_Button action={onImageRemove}
                                           className="delete no-margin-top">
                                    {icons.close}
                                </SC_Button>
                            </figure>
                        }

                    </div>

                    <div>
                        {wp.element.createElement(titleTag, {style: {margin: 0}},
                            <PlainText
                                placeholder={_x('Enter your recipe title here…', 'Title of the recipe,', 'structured-content')}
                                value={title}
                                className="wp-block-structured-content-recipe__title"
                                tag={titleTag}
                                onChange={(value) => changeTitle(value)}
                                keepplaceholderonfocus="true"
                            />,
                        )}


                        <div className="description" itemProp="text">
                            <RichText
                                placeholder={_x('Enter your recipe description here…', 'Details/description about the recipe offer.', 'structured-content')}
                                value={description}
                                className="wp-block-structured-content-recipe__text"
                                onChange={(value) => setAttributes({description: value})}
                                keepplaceholderonfocus="true"
                            />
                        </div>

                        {showPrintButton && (
                            <div style={{
                                marginTop: '1rem',
                                marginBottom: '1rem',
                            }}>
                                <SC_Button
                                    action={() => alert(_x('I am a print button.', 'Print button.', 'structured-content'))}>
                                    {printButtonText}
                                </SC_Button>
                            </div>
                        )}

                        <div className="sc_grey-box" style={{marginTop: '1rem'}}>

                            <div style={{
                                display: 'grid',
                                gridTemplateColumns: '1fr 1fr 1fr',
                                gridGap: '1rem',
                            }}>

                                {/* Recipe Cuisine */}
                                <div className="sc_input-group" itemProp="recipeCuisine">
                                    <div className="sc_input-label">
                                        {_x('Cuisine', 'Cuisine of the recipe.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={_x('Enter your recipe cuisine here…', 'Cuisine of the recipe.', 'structured-content')}
                                        value={recipeCuisine}
                                        className="wp-block-structured-content-recipe__text"
                                        onChange={(value) => setAttributes({recipeCuisine: value})}
                                        keepplaceholderonfocus="true"
                                    />
                                </div>

                                {/* Recipe Category */}
                                <div className="sc_input-group" itemProp="recipeCategory">
                                    <div className="sc_input-label">
                                        {_x('Category', 'Category of the recipe.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={_x('Enter your recipe category here…', 'Category of the recipe.', 'structured-content')}
                                        value={recipeCategory}
                                        className="wp-block-structured-content-recipe__text"
                                        onChange={(value) => setAttributes({recipeCategory: value})}
                                        keepplaceholderonfocus="true"
                                    />
                                </div>

                                {/* Recipe Yield */}
                                <div className="sc_input-group" itemProp="recipeYield">
                                    <div className="sc_input-label">
                                        {_x('Yield', 'Yield of the recipe.', 'structured-content')}
                                    </div>
                                    <TextControl
                                        placeholder={_x('Enter your recipe yield here…', 'Yield of the recipe.', 'structured-content')}
                                        value={recipeYield}
                                        className="wp-block-structured-content-recipe__text"
                                        onChange={(value) => setAttributes({recipeYield: value})}
                                        keepplaceholderonfocus="true"
                                    />
                                </div>
                            </div>

                            <div style={{
                                display: 'grid',
                                gridTemplateColumns: '1fr 1fr 1fr',
                                gridGap: '1rem',
                            }}>

                                {/* Prep Time */}
                                <div className="sc_input-group" itemProp="prepTime">
                                    <div className="sc_input-label">
                                        {_x('Prep Time', 'Prep time of the recipe.', 'structured-content')}
                                    </div>
                                    <DurationInput
                                        duration={prepTime}
                                        onChange={(value) => setAttributes({prepTime: value})}
                                    />
                                </div>

                                {/* Cook Time */}
                                <div className="sc_input-group" itemProp="cookTime">
                                    <div className="sc_input-label">
                                        {_x('Cook Time', 'Cook time of the recipe.', 'structured-content')}
                                    </div>

                                    <DurationInput
                                        duration={cookTime}
                                        onChange={(value) => setAttributes({cookTime: value})}
                                    />

                                </div>

                                {/* Total Time */}
                                <div className="sc_input-group" itemProp="totalTime">
                                    <div className="sc_input-label">
                                        {_x('Total Time', 'Total time of the recipe.', 'structured-content')}
                                    </div>
                                    <DurationInput
                                        duration={totalTime}
                                        onChange={(value) => setAttributes({totalTime: value})}
                                    />
                                </div>

                            </div>

                        </div>
                        <div className="sc_grey-box" style={{marginTop: '1rem'}}>


                            {/* Nutrition */}
                            <div className="sc_input-group" itemProp="nutrition">
                                <div className="sc_input-label">
                                    {_x('Nutrition', 'Nutrition of the recipe.', 'structured-content')}
                                </div>

                                {/* Loop through nutrition */}
                                {nutrition.map((item, index) => {
                                    const {type, value} = item;

                                    return (
                                        <div key={index}
                                             style={{
                                                 display: 'grid',
                                                 gridTemplateColumns: '1fr 1fr 2rem',
                                                 gridGap: '1rem',
                                             }}>
                                            {/* Nutrition Type */}
                                            <div className="sc_input-group" itemProp="nutrition">
                                                <div className="sc_input-label">
                                                    {_x('Type', 'Type of the nutrition.', 'structured-content')}
                                                </div>
                                                <SelectControl
                                                    value={type}
                                                    options={nutritionTypes}
                                                    onChange={(value) => onNutritionTypeChange(value, index)}
                                                />
                                            </div>

                                            {/* Nutrition Value */}
                                            <div className="sc_input-group" itemProp="nutrition">
                                                <div className="sc_input-label">
                                                    {_x('Value', 'Value of the nutrition.', 'structured-content')}
                                                </div>
                                                <TextControl
                                                    placeholder={_x('Enter your nutrition value here…', 'Value of the nutrition.', 'structured-content')}
                                                    value={value}
                                                    className="wp-block-structured-content-recipe__text"
                                                    onChange={(value) => onNutritionValueChange(value, index)}
                                                    keepplaceholderonfocus="true"
                                                />
                                            </div>

                                            {/* Delete Nutrition */}
                                            <SC_Button
                                                style={{
                                                    width: '2rem',
                                                    height: '2rem',
                                                }}
                                                action={() => onNutritionRemove(index)}
                                            >
                                                {icons.close}
                                            </SC_Button>


                                        </div>
                                    )
                                })}

                                {/* Add Nutrition */}
                                <SC_Button action={() => onNutritionAdd()}>
                                    {_x('Add Nutrition',
                                        'Add a nutrition item to your recipe.',
                                        'structured-content')}
                                </SC_Button>

                            </div>

                        </div>
                        <div className="sc_grey-box" style={{marginTop: '1rem'}}>

                            {/* Ingredients */}
                            <div className="sc_input-group" itemProp="recipeIngredient">
                                <div className="sc_input-label">
                                    {_x('Ingredients', 'Ingredients of the recipe.', 'structured-content')}
                                </div>

                                {/* Loop through ingredients */}
                                <div style={{
                                    display: 'flex',
                                    flexDirection: 'column',
                                    gap: '.5rem',
                                    marginTop: '.25rem'
                                }}>
                                    {recipeIngredient.map((item, index) => {
                                        return (
                                            <div key={index}
                                                 style={{
                                                     display: 'flex',
                                                     alignItems: 'start',
                                                     justifyContent: 'space-between',
                                                     width: '100%',
                                                 }}
                                            >
                                                <div style={{
                                                    flexGrow: 1,
                                                    marginRight: '1rem',
                                                    width: '100%',
                                                    display: 'flex',
                                                    alignItems: 'start',
                                                    justifyContent: 'start',
                                                }}>
                                                    {ingredientsAsChecklist && (
                                                        <CheckboxControl
                                                            disabled
                                                            onChange={() => {
                                                            }}
                                                        />
                                                    )}
                                                    <div style={{
                                                        flexGrow: 1,
                                                        width: '100%',
                                                    }}>
                                                    <TextControl
                                                        placeholder={_x('Enter your ingredient here…', 'Ingredient of the recipe.', 'structured-content')}
                                                        value={item}
                                                        className="wp-block-structured-content-recipe__text"
                                                        onChange={(value) => onIngredientChange(value, index)}
                                                        keepplaceholderonfocus="true"
                                                    />
                                                    </div>
                                                </div>
                                                <div style={{
                                                    flexShrink: 0,
                                                }}>
                                                    <SC_Button style={{
                                                        width: '2rem',
                                                        marginTop: 0,
                                                    }} action={() => onIngredientRemove(index)}>
                                                        {icons.close}
                                                    </SC_Button>
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>

                                {/* Add Ingredient */}
                                <SC_Button action={() => onIngredientAdd()}>
                                    {_x('Add Ingredient',
                                        'Add an ingredient to your recipe.',
                                        'structured-content')}
                                </SC_Button>

                            </div>

                        </div>
                        <div className="sc_grey-box" style={{marginTop: '1rem'}}>
                            {/* Instructions */}
                            <div className="sc_input-group" itemProp="recipeInstructions">
                                <div className="sc_input-label">
                                    {_x('Instructions', 'Instructions of the recipe.', 'structured-content')}
                                </div>

                                {/* Loop through instructions */}
                                <ol style={{
                                    paddingLeft: '1em',
                                }}>
                                    {recipeInstructions.map((item, index) => {
                                        return (
                                            <li key={index} style={{
                                                marginBottom: '1.5rem'
                                            }}>
                                                <span style={{
                                                    display: 'flex',
                                                    flexDirection: 'column'
                                                }}>
                                                    <span style={{
                                                        display: 'flex',
                                                        flexDirection: 'row',
                                                        alignItems: 'start',
                                                        justifyContent: 'space-between',
                                                        width: '100%',
                                                        gap: '1rem',
                                                    }}>
                                                        <span style={{
                                                            width: '100%',
                                                            display: 'inline-block',
                                                        }}><TextControl
                                                            placeholder={_x('Enter your instruction here…', 'Instruction of the recipe.', 'structured-content')}
                                                            value={item.text}
                                                            className="wp-block-structured-content-recipe__text"
                                                            onChange={(value) => onInstructionChange(value, index)}
                                                            keepplaceholderonfocus="true"
                                                        /></span>
                                                            <SC_Button style={{
                                                                width: '2rem',
                                                                marginTop: 0,
                                                            }} action={() => onInstructionRemove(index)}>
                                                                {icons.close}
                                                            </SC_Button>
                                                    </span>

                                                    {item.image ? (
                                                        <Fragment>
                                                            <div key={item.image.id} className="image-wrapper"
                                                                 style={{
                                                                     marginTop: '0.5rem',
                                                                     marginBottom: 0,
                                                                 }}>
                                                                <figure
                                                                    className="wp-block-structured-content-recipe__figure"
                                                                    style={{
                                                                        lineHeight: 0,
                                                                        margin: 0,
                                                                        display: 'block',
                                                                        textAlign: 'left',
                                                                        padding: 8
                                                                    }}
                                                                >
                                                                    <img
                                                                        src={item.image.sizes[instructionImageSize].url || item.image.url}
                                                                        style={{
                                                                            width: "auto",
                                                                            height: "auto",
                                                                            maxWidth: "100%",
                                                                        }}
                                                                    />
                                                                </figure>
                                                            </div>
                                                            <SC_Button action={() => onInstructionImageRemove(index)}>
                                                                {_x('Remove Image from Instruction', 'Remove an image from your instruction.', 'structured-content')}
                                                            </SC_Button>
                                                        </Fragment>
                                                    ) : (
                                                        <MediaUpload
                                                            onSelect={(media) => onInstructionImageChange(media, index)}
                                                            type="image"
                                                            render={({open}) => (
                                                                <SC_Button action={open} style={{
                                                                    marginTop: '0.5rem',
                                                                    marginBottom: 0
                                                                }}>
                                                                    {_x('Add Image',
                                                                        'Add an image to your instruction.',
                                                                        'structured-content')}
                                                                </SC_Button>
                                                            )}
                                                        />
                                                    )}

                                                </span>


                                            </li>
                                        )
                                    })}
                                </ol>

                                {/* Add Instruction */}
                                <SC_Button action={() => onInstructionAdd()}>
                                    {_x('Add Instruction',
                                        'Add an instruction to your recipe.',
                                        'structured-content')}
                                </SC_Button>


                            </div>
                        </div>


                        {hasVideo ? (
                            <div className="sc_grey-box" style={{marginTop: 24, position: 'relative'}}>
                                <div className="sc_white-box">
                                    <SC_Button action={() => setAttributes({hasVideo: !hasVideo, video: videoDefaults})}
                                               className="delete no-margin-top">
                                        {icons.close}
                                    </SC_Button>

                                    <VideoInput video={video} isSelected={isSelected} onChange={onVideoChange}
                                                titleTag="h4"/>
                                </div>
                            </div>
                        ) : (
                            !hasVideo &&
                            <div className="sc_grey-box" style={{marginTop: 24, position: 'relative'}}>
                                <Placeholder
                                    icon="video-alt3"
                                    instructions={_x('Add a video to your recipe by clicking the button below.', 'Add a video to your recipe.', 'structured-content')}
                                    label={_x('Add Video', 'Add a video to your recipe.', 'structured-content')}
                                />
                                <SC_Button action={() => setAttributes({hasVideo: !hasVideo})}>
                                    {_x('Add Video',
                                        'Add a video to your recipe.',
                                        'structured-content')}
                                </SC_Button>
                            </div>
                        )}

                    </div>
                </section>
            </Fragment>

        ];
    },

    save: (props) => {
        return null
    }
});
