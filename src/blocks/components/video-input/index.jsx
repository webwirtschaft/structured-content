import SC_Button from "../sc-buttons/index.js";
import Tabs from "../tabs/index.jsx";

import {_x} from '@wordpress/i18n';
import ReactPlayer from 'react-player'

const {Component, Fragment} = wp.element;
const {
    TextControl,
    PanelRow,
    PanelBody,
    ToggleControl,
    BaseControl
} = wp.components;
const {MediaUpload, PlainText, RichText, InspectorControls} = wp.blockEditor;

class VideoInput extends Component {
    constructor(props) {
        super(props);

        const {video, titleTag = 'h2', isSelected = true} = this.props;
        const {name, description, thumbnailUrl, contentUrl, mediaVideo, settings} = video || {};

        const initialTab = mediaVideo ? 'media' : 'url';

        this.state = {
            name: name || '',
            description: description || '',
            thumbnailUrl: thumbnailUrl || '',
            contentUrl: contentUrl || '',
            titleTag: titleTag || 'h2',
            mediaVideo: mediaVideo || undefined,
            initialTab: initialTab || 'url',
            settings: settings || {
                autoplay: false,
                loop: false,
                muted: false,
                controls: true,
            },
            type: 'self-hosted'
        };

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onSelectMedia = this.onSelectMedia.bind(this);
        this.onChangeContentUrl = this.onChangeContentUrl.bind(this);
        this.onChangeThumbnailUrl = this.onChangeThumbnailUrl.bind(this);
        this.removeMedia = this.removeMedia.bind(this);
        this.onTabChange = this.onTabChange.bind(this);
        this.checkVideoType = this.checkVideoType.bind(this);
    }

    componentDidMount() {
        this.checkVideoType(this.state.contentUrl);
    }

    triggerChange() {
        const {onChange} = this.props;
        const {
            name,
            description,
            thumbnailUrl,
            contentUrl,
            mediaVideo,
            settings
        } = this.state;

        if (onChange) {
            onChange({
                name,
                description,
                thumbnailUrl,
                contentUrl,
                mediaVideo,
                settings
            });
        }
    }

    onChangeName(name) {
        this.setState({name}, () => {
            this.triggerChange();
        });
    }

    onChangeDescription(description) {
        this.setState({description}, () => {
            this.triggerChange();
        });
    }

    removeMedia() {
        this.setState({
            mediaVideo: undefined,
            contentUrl: '',
            thumbnailUrl: '',
            type: 'self-hosted',
            settings: {
                autoplay: false,
                loop: false,
                muted: false,
                controls: true,
            }
        }, () => {
            this.triggerChange();
        });
    }

    onSelectMedia(mediaVideo) {
        this.setState({
            contentUrl: '',
            thumbnailUrl: '',
            type: 'self-hosted',
            mediaVideo
        }, () => {
            this.triggerChange();
        });
    }

    checkVideoType(contentUrl = null) {
        const videoUrl = contentUrl
        const videoExt = videoUrl.split('.').pop();

        const videoTypes = [
            'mp4',
            'ogg',
            'webm',
        ];

        if (videoTypes.includes(videoExt) || videoUrl === '') {
            this.setState({
                type: 'self-hosted'
            });
        } else {
            this.setState({
                type: 'external'
            });
        }
    }

    onChangeContentUrl(contentUrl) {
        this.setState({mediaVideo: undefined, contentUrl}, () => {
            this.triggerChange();
        });

        this.checkVideoType(contentUrl);

    }

    onChangeThumbnailUrl(thumbnailUrl) {
        this.setState({mediaVideo: undefined, thumbnailUrl}, () => {
            this.triggerChange();
        });
    }

    onTabChange() {
        this.setState({
            mediaVideo: undefined,
            contentUrl: '',
            thumbnailUrl: '',
            type: 'self-hosted',
            settings: {
                autoplay: false,
                loop: false,
                muted: false,
                controls: true,
            },
        }, () => {
            this.triggerChange();
        });
    }

    render() {
        const {
            name,
            description,
            thumbnailUrl,
            contentUrl,
            mediaVideo,
            initialTab,
            titleTag = 'h2',
            type = 'self-hosted',
            isSelected = true
        } = this.state;


        const tabs = [
            {
                name: 'url',
                title: _x('URL', 'URL of the video,', 'structured-content'),
                content: (
                    <div>
                        <TextControl
                            label={_x('Video URL', 'URL of the video,', 'structured-content')}
                            value={contentUrl}
                            onChange={this.onChangeContentUrl}
                            type="url"
                        />
                        {type === 'self-hosted' && (
                            <TextControl
                                label={_x('Poster URL', 'Thumbnail URL of the video,', 'structured-content')}
                                value={thumbnailUrl}
                                onChange={this.onChangeThumbnailUrl}
                                type="url"
                            />
                        )}

                        {contentUrl && (
                            <div className="wp-block-structured-content-video__preview" style={{
                                position: 'relative',
                            }}>
                                {/* div with aspect ratio 16:9 */}
                                <div style={{
                                    position: 'relative',
                                    paddingBottom: '56.25%',
                                    height: 0,
                                    overflow: 'hidden',
                                }}/>
                                <ReactPlayer
                                    url={contentUrl}
                                    controls={true}
                                    width="100%"
                                    height="100%"
                                    style={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                    }}
                                    light={thumbnailUrl || undefined}
                                />

                            </div>
                        )}

                    </div>
                ),
            },
            {
                name: 'media',
                title: _x('Media', 'Media of the video,', 'structured-content'),
                content: (
                    <div>
                        {mediaVideo ? (
                            <Fragment>
                                {isSelected && (
                                    <InspectorControls>
                                        <PanelBody title={_x('Video Settings', 'Video settings', 'structured-content')} initialOpen={ false }>
                                            <PanelRow>
                                                <BaseControl
                                                    className="wp-block-structured-content-video__settings"
                                                    label={_x('Recipe Video Settings', 'Recipe video settings', 'structured-content')}
                                                >
                                                    <ToggleControl
                                                        label={_x('Autoplay', 'Autoplay video,', 'structured-content')}
                                                        checked={this.state.settings.autoplay}
                                                        onChange={() => {
                                                            this.setState({
                                                                settings: {
                                                                    ...this.state.settings,
                                                                    autoplay: !this.state.settings.autoplay
                                                                }
                                                            }, () => {
                                                                this.triggerChange();
                                                            });
                                                        }}
                                                    />

                                                    <ToggleControl
                                                        label={_x('Loop', 'Loop video,', 'structured-content')}
                                                        checked={this.state.settings.loop}
                                                        onChange={() => {
                                                            this.setState({
                                                                settings: {
                                                                    ...this.state.settings,
                                                                    loop: !this.state.settings.loop
                                                                }
                                                            }, () => {
                                                                this.triggerChange();
                                                            });
                                                        }}
                                                    />

                                                    <ToggleControl
                                                        label={_x('Muted', 'Muted video,', 'structured-content')}
                                                        checked={this.state.settings.muted}
                                                        onChange={() => {
                                                            this.setState({
                                                                settings: {
                                                                    ...this.state.settings,
                                                                    muted: !this.state.settings.muted
                                                                }
                                                            }, () => {
                                                                this.triggerChange();
                                                            });
                                                        }}
                                                    />

                                                    <ToggleControl
                                                        label={_x('Controls', 'Controls video,', 'structured-content')}
                                                        checked={this.state.settings.controls}
                                                        onChange={() => {
                                                            this.setState({
                                                                settings: {
                                                                    ...this.state.settings,
                                                                    controls: !this.state.settings.controls
                                                                }
                                                            }, () => {
                                                                this.triggerChange();
                                                            });
                                                        }}
                                                    />
                                                </BaseControl>
                                            </PanelRow>
                                        </PanelBody>
                                    </InspectorControls>
                                )}

                                <div className="wp-block-structured-content-video__preview" style={{
                                    position: 'relative',
                                }}>
                                    <video
                                        src={mediaVideo.url}
                                        controls={this.state.settings.controls}
                                        poster={thumbnailUrl}
                                        className="wp-block-structured-content-video__preview-video"
                                        width="100%"
                                    />

                                    <SC_Button action={this.removeMedia}>
                                        {_x('Remove Video', 'Remove video from media library', 'structured-content')}
                                    </SC_Button>
                                </div>
                            </Fragment>

                        ) : (
                            <>
                                <MediaUpload
                                    onSelect={this.onSelectMedia}
                                    allowedTypes={['video/mp4', 'video/ogg', 'video/webm']}
                                    render={({open}) => (
                                        <SC_Button action={open} className="no-margin-top">
                                            {_x('Select Video', 'Select video from media library', 'structured-content')}
                                        </SC_Button>
                                    )}
                                />
                            </>
                        )}
                    </div>
                ),
            },
        ];


        return (
            <div>
                {wp.element.createElement(titleTag, {},
                    <PlainText
                        placeholder={_x('Enter your video title here…', 'Title of the video,', 'structured-content')}
                        value={name}
                        className="wp-block-structured-content-video__title"
                        tag={titleTag}
                        onChange={this.onChangeName}
                        keepplaceholderonfocus="true"
                    />,
                )}

                <div style={{
                    marginBottom: '1rem'
                }}>
                    <RichText
                        placeholder={_x('Enter your Video description here...', 'Description of the video,', 'structured-content')}
                        value={description}
                        onChange={this.onChangeDescription}
                        keepplaceholderonfocus="true"
                    />
                </div>

                <Tabs tabs={tabs} initialTab={initialTab} onTabChange={this.onTabChange}/>


            </div>
        );
    }
}

export default VideoInput;
