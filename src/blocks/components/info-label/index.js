import React, {Component} from 'react';

import {_x} from '@wordpress/i18n';

import {icons} from '../../../util/icons.js';

class InfoLabel extends Component {
    constructor(props) {
        super(props);
    }

    /**
     * generate a random key
     * @return {string} random key
     */
    generateKey() {
        return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    }


    render() {
        return (
            <a key={this.generateKey()} href={this.props.url} target="_blank" className="sc_info-label"
               rel="noreferrer">
                {icons.info} <span>{_x('Info', 'Info label', 'structured-content')}</span>
            </a>
        );
    }
}

export default InfoLabel;
