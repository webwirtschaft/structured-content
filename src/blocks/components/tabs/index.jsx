import classNames from 'classnames';

const {useState, useEffect} = wp.element;

const TabButton = ({children, className, action, key}) => (
    <button type="button" className={classNames('sc-tab-nav__button', className)} onClick={action} key={key}>
        {children}
    </button>
);

const Tabs = ({tabs, initialTab, onTabClick = null, onTabChange = null}) => {

    const [activeTab, setActiveTab] = useState(0);


    // Set the initial tab only once
    useEffect(() => {
        if (initialTab) {
            // set active tab by find index
            const index = tabs.findIndex((tab) => tab.name === initialTab);
            setActiveTab(index);
        }
    }, []);

    const handleTabClick = (index) => {
        if (onTabClick && typeof onTabClick === 'function') {
            onTabClick(index);
        }

        if (onTabChange && typeof onTabChange === 'function') {
            if (index !== activeTab) {
                onTabChange(index);
            }
        }

        setActiveTab(index);
    };

    return (
        <div className="sc-tabs">
            <div className="sc-tab-nav">
                {tabs.map((tab, index) => (
                    <TabButton
                        key={tab.id}
                        className={index === activeTab ? 'active' : ''}
                        action={() => handleTabClick(index)}
                    >
                        {tab.title}
                    </TabButton>
                ))}
            </div>
            <div className="sc-tab-content">
                {tabs.length > 0 && (
                    tabs.map((tab, index) => (
                        <div
                            key={tab.id}
                            className={classNames('sc-tab-content__tab', index === activeTab ? 'active' : '')}
                        >
                            {tab.content}
                        </div>
                    ))
                )}
                {tabs.length === 0 && <p>No tabs to display.</p>}
            </div>
        </div>
    );
};

export default Tabs;
