import {_x} from '@wordpress/i18n';
import React from 'react';

const DividerWithWord = (props) => {
    const {word, color, size} = props;

    const displayWord = word || _x('OR', 'Word to display in the divider', 'structured-content');

    const styles = {
        wrapper: {
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        },
        divider: {
            width: '100%',
            height: '1px',
            backgroundColor: color || 'black'
        },
        word: {
            padding: '0 10px',
            fontSize: size || '.75em',
            fontWeight: 'bold',
            color: color || 'black'
        }
    }
    return (
        <div style={styles.wrapper}>
            <div style={styles.divider}></div>
            <div style={styles.word}>{displayWord}</div>
            <div style={styles.divider}></div>
        </div>
    );
}


export default DividerWithWord;
