import React, {Component} from 'react';

import {icons} from '../../../util/icons.js';

class VisibleLayer extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <button className="visibleButton" onClick={this.props.onClick}>
                {this.props.visible ? icons.openEye : icons.closedEye}
            </button>
        );
    }
}

export default VisibleLayer;
