import {_n, _x} from '@wordpress/i18n';
import SC_Button from '../sc-buttons'

const {Component, Fragment} = wp.element;
const {TextControl, Dropdown, Icon, Flex, FlexBlock, FlexItem} = wp.components;

class DurationInput extends Component {
    constructor(props) {
        super(props);

        const {duration} = this.props;
        const {days, hours, minutes} = this.parseDuration(duration);

        this.state = {
            days: days || '0',
            hours: hours || '0',
            minutes: minutes || '0',
        };

        this.onChangeDays = this.onChangeDays.bind(this);
        this.onChangeHours = this.onChangeHours.bind(this);
        this.onChangeMinutes = this.onChangeMinutes.bind(this);
    }


    parseDuration(duration) {
        const regex = /P([\d]+)DT([\d]+)H([\d]+)M/;
        const match = duration ? duration.match(regex) : null;

        if (match) {
            const days = match[1];
            const hours = match[2];
            const minutes = match[3];
            return {
                days: parseInt(days),
                hours: parseInt(hours),
                minutes: parseInt(minutes)
            };
        }

        return {};
    }

    formatDuration() {
        const {days, hours, minutes} = this.state;

        if (!days && !hours && !minutes) {
            return null;
        }

        return `P${days || '0'}DT${hours || '0'}H${minutes || '0'}M`;
    }

    onChangeDays(days) {
        this.setState({days}, this.triggerChange);
    }

    onChangeHours(hours) {
        this.setState({hours}, this.triggerChange);
    }

    onChangeMinutes(minutes) {
        this.setState({minutes}, this.triggerChange);
    }

    triggerChange() {
        const {onChange} = this.props;
        const duration = this.formatDuration();

        if (onChange) {
            onChange(duration, this.state);
        }
    }

    readableDuration() {
        const {days, hours, minutes} = this.state;
        let duration = '';

        if (days && days > 0) {
            duration += `${days} ${_n('day', 'days', days, 'structured-content')} `;
        }

        if (hours && hours > 0) {
            duration += `${hours} ${_n('hour', 'hours', hours, 'structured-content')} `;
        }

        if (minutes && minutes > 0) {
            duration += `${minutes} ${_n('minute', 'minutes', minutes, 'structured-content')} `;
        }

        return duration.trim();
    }

    render() {
        const {days, hours, minutes} = this.state;

        return (
            <div style={{width: '100%'}}>
                <Dropdown
                    style={{width: '100%'}}
                    renderToggle={({isOpen, onToggle}) => (
                        <div style={{
                            display: 'flex',
                            alignItems: 'start',
                            gap: '1rem'
                        }}>
                            <div style={{flex: 1, cursor: 'pointer', lineHeight: 0}} onClick={onToggle}>
                                <TextControl
                                    value={this.readableDuration()}
                                    style={{pointerEvents: 'none'}}
                                    __nextHasNoMarginBottom
                                />
                            </div>
                            <SC_Button
                                style={{width: '2rem', height: '2rem', margin: 0}}
                                action={onToggle}
                                aria-expanded={isOpen}
                            >
                                <Icon icon="edit" size={16}/>
                            </SC_Button>
                        </div>
                    )}
                    renderContent={() =>
                        <div style={{
                            padding: '.5em 1em',
                            minWidth: '16.75em',
                        }}>
                            <Flex gap={4}>
                                <FlexItem>
                                    <TextControl
                                        label={_x('Days', 'duration', 'structured-content')}
                                        value={days}
                                        pattern="[0-9]*"
                                        onChange={this.onChangeDays}
                                        type="text"
                                    />
                                </FlexItem>
                                <FlexItem>
                                    <TextControl
                                        label={_x('Hours', 'duration', 'structured-content')}
                                        value={hours}
                                        pattern="[0-9]*"
                                        onChange={this.onChangeHours}
                                        type="text"
                                    />
                                </FlexItem>
                                <FlexItem>
                                    <TextControl
                                        label={_x('Minutes', 'duration', 'structured-content')}
                                        value={minutes}
                                        pattern="[0-9]*"
                                        onChange={this.onChangeMinutes}
                                        type="text"
                                    />
                                </FlexItem>
                            </Flex>
                        </div>
                    }
                />

            </div>
        );
    }
}

export default DurationInput;
