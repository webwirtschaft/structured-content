import path from 'path';
import { fileURLToPath } from 'url';
import scss from 'rollup-plugin-scss';
import babel from '@rollup/plugin-babel';
import { nodeResolve } from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import { terser } from 'rollup-plugin-terser';
import postcss from 'rollup-plugin-postcss';
import replace from '@rollup/plugin-replace';
import alias from "@rollup/plugin-alias";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let cache;

const createJSConfig = (input, output, useSass = false, sassOptions = {}) => {
    const plugins = [
        commonjs(),
        babel({
            presets: [['@babel/preset-env']],
            plugins: ['@babel/plugin-transform-runtime'],
            exclude: 'node_modules/**',
            babelHelpers: 'runtime',
        }),
        nodeResolve({ browser: true }),
        useSass ? scss({
            ...sassOptions,
            sourceMapEmbed: false, // Deaktiviert für Produktionsbuild
            watch: 'src/style', // Engere Überwachung
        }) : postcss({ extensions: ['.css'], minimize: true }),
    ];

    return {
        input,
        output: {
            file: output,
            format: 'iife',
            plugins: [terser()],
        },
        cache, // Caching aktivieren
        external: ['tinymce'],
        plugins,
    };
};


const createJSXConfig = (input, output) => {
    return {
        input,
        output: {
            file: output,
            format: 'iife',
            plugins: [
                terser({
                    keep_fnames: true,
                }),
            ],
            globals: {
                react: 'React',
                'react-dom': 'ReactDOM',
                '@wordpress/block-editor': 'wp.blockEditor',
                '@wordpress/editor': 'wp.editor',
                '@wordpress/element': 'wp.element',
                '@wordpress/i18n': 'wp.i18n',
                '@wordpress/components': 'wp.components',
                '@wordpress/blocks': 'wp.blocks',
                '@wordpress/data': 'wp.data',
                '@wordpress/date': 'wp.date',
                lodash: 'lodash',
                wp: 'wp',
            },
        },
        external: ['wp', 'wp.element', 'wp.components', 'wp.blocks', 'wp.data', 'wp.i18n', 'wp.date', 'wp.blockEditor', 'wp.editor', '_'],
        plugins: [
            // https://github.com/rollup/rollup-plugin-node-resolve
            nodeResolve({
                browser: true,
            }),
            commonjs({
                include: /node_modules/,
            }),
            alias({
                /**
                 * For custom files extension you might want to add "customerResolver"
                 * https://github.com/rollup/plugins/tree/master/packages/alias#custom-resolvers
                 *
                 * By doing that this plugin can read different kind of files.
                 */
                entries: [
                    {
                        find: "util",
                        replacement: path.resolve(__dirname, "src/util"),
                    }
                ],
            }),
            babel({
                presets: [
                    '@wordpress/babel-preset-default',
                    '@babel/preset-react',
                ],
                plugins: [
                    '@babel/plugin-transform-runtime'
                ],
                exclude: 'node_modules/!**',
                babelHelpers: 'runtime',
                compact: true,
            }),
            replace({
                'process.env.NODE_ENV': JSON.stringify('production'),
                preventAssignment: true,
            }),
            scss({
                fileName: 'blocks.editor.build.css',
                // see options here : https://www.npmjs.com/package/node-sass
                outputStyle: 'expanded',
                sourceComments: true,
                sourceMapEmbed: true,

            }),
        ],
    };
};

// Additional configuration for generating a print.scss file
const createPrintSCSSConfig = () => {
    return createJSConfig('./src/style/print.scss', 'dist/print.css', true, {
        outputStyle: 'expanded',
        sourceComments: false, // Set to false for production
        fileName: 'print.css',
    });
};


export default [
    createJSConfig('./src/tinymce/tinymce.js', 'dist/tinymce.js'),
    createJSConfig('./src/app.js', 'dist/app.build.js', true, {
        outputStyle: 'expanded',
        sourceComments: false, // Set to false for production
        fileName: 'blocks.style.build.css',
    }),
    createJSXConfig('./src/blocks.js', 'dist/blocks.build.js'),
    createPrintSCSSConfig(),
];
